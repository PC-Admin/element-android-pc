
# Perthchat App

The Perthchat App is a re-branding of the popular [Element Android](https://github.com/vector-im/element-android) client.

Perthchat.org is a Matrix service run for Perth, Western Australia. Started in 2017 it’s one of the larger Matrix instances in Australia. This service was created to provide a secure, open source and ad-free messaging experience. Matrix servers interconnect allowing users to make friends and join rooms with people from all over the world.

## Issues

For bug reports feel free to create an issue on [our issue tracker](https://gitlab.com/perthchat.org/element-android-perthchat/-/issues). Keep in mind though this is nearly exactly the same code as the upstream [Element Android](https://github.com/vector-im/element-android) client and the bug you're experiencing might already be documented there.